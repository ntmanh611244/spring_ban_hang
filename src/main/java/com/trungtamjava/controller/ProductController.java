package com.trungtamjava.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.trungtamjava.model.ProductDTO;
import com.trungtamjava.service.ProductService;

@Controller
public class ProductController {
	private static Logger logger = Logger.getLogger(ProductController.class);
	
	@Autowired
	private ProductService pService;
	
	@Autowired
	private MessageSource messageSource;
	
	@RequestMapping(value = {"/danh-sach-san-pham"})
	public String getAllProduct(HttpServletRequest request) {
		pService.getAllProduct();
		request.setAttribute("products", pService.getAllProduct());
		request.setAttribute("msg",messageSource.getMessage("product.name", null, null));
		return "listProducts";
	}
	
	@RequestMapping(value = {"/them-san-pham"}, method = RequestMethod.GET)
	public String addProduct(HttpServletRequest request) {
		request.setAttribute("product",new ProductDTO());
		return "addProduct";
	}
	
	@RequestMapping(value = {"/them-san-pham"}, method = RequestMethod.POST)
	public String addProduct(HttpServletRequest request, @ModelAttribute("product") ProductDTO pDto) {
		pService.addProduct(pDto);
		return "redirect:/danh-sach-san-pham";
	}
}
