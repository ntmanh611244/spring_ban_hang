package com.trungtamjava.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.trungtamjava.model.UserDTO;
import com.trungtamjava.service.UserService;
import com.trungtamjava.validator.UserValidator;

@Controller
@RequestMapping("/admin")
public class UserController {
	private static Logger logger = Logger.getLogger(UserController.class);
	
	@Autowired
	UserService userService;
	
	@Autowired
	UserValidator userValidator;
	
	@RequestMapping(value = {"/danh-sach-khach-hang"}, method = RequestMethod.GET)
	public String getAllUser(HttpServletRequest request, HttpSession session) {
		logger.info("Thong tin khach hang");
		List<UserDTO> users = userService.getAllUser();
		request.setAttribute("users", users);
		
		session.setAttribute("users", users);
		//add
		request.setAttribute("user",new UserDTO());
		return "listUser";
	}
	@RequestMapping(value = {"/chi-tiet-khach-hang/{userId}"}, method = RequestMethod.GET)
	public String viewUser(HttpServletRequest request, 
			@PathVariable(name="userId") int userId) {
		
		request.setAttribute("user", userService.getUserById(userId));
		
		return "viewUser";
	}
	
	@RequestMapping(value = {"/them-khach-hang"}, method = RequestMethod.GET)
	public String addUser(HttpServletRequest request) {
		//List<UserDTO> users = userService.getAllUser();
		request.setAttribute("user",new UserDTO());
		
		return "addUser";
	}
	
	@RequestMapping(value="/them-khach-hang", method = RequestMethod.POST)
	public String addUser(HttpServletRequest request, @ModelAttribute("user") UserDTO user,
			BindingResult bindingResult) {
		userValidator.validate(user, bindingResult);
		if(bindingResult.hasErrors()) {	
			return "addUser"; // tro den nam trong tiles
		}
		
		userService.addUser(user);
		return "redirect:/admin/danh-sach-khach-hang";
	}
	
	@RequestMapping(value = {"/xoa-khach-hang/{userId}"}, method = RequestMethod.GET)
	public String deleteUser(HttpServletRequest request, 
			@PathVariable(name="userId") int userId) {
		
		userService.deleteUser(userId);
		
		return "redirect:/admin/danh-sach-khach-hang";
	}
	
	@RequestMapping(value = {"/sua-khach-hang/{userId}"}, method = RequestMethod.GET)
	public String editUser(HttpServletRequest request, 
			@PathVariable(name="userId") int userId) {
		
		request.setAttribute("user", userService.getUserById(userId));
		
		return "editUser";
	}
	
	@RequestMapping(value="/sua-khach-hang", method = RequestMethod.POST)
	public String editUser(HttpServletRequest request, @ModelAttribute("user") UserDTO user,
			BindingResult bindingResult) {
		userValidator.validate(user, bindingResult);
		if(bindingResult.hasErrors()) {	
			return "editUser";
		}
		
		userService.updateUser(user);
		return "redirect:/admin/danh-sach-khach-hang";
	}
}
