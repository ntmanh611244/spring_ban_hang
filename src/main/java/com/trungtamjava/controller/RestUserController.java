package com.trungtamjava.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.trungtamjava.model.UserDTO;
import com.trungtamjava.service.UserService;
import com.trungtamjava.validator.UserValidator;

@RestController//Chi su dung cho rest api thoi
public class RestUserController {
	@Autowired
	UserService userService;
	
	@Autowired
	UserValidator userValidator;
	
	@RequestMapping(value = {"/list-user"}, method = RequestMethod.GET) //Tra ve dang web service
	public List<UserDTO> listUser(HttpServletRequest request) {
		
		List<UserDTO> users = userService.getAllUser();
		
		return users;
	}
	
	@RequestMapping(value="/add-user", method = RequestMethod.POST)
	@ResponseStatus(code = HttpStatus.CREATED)
	public void addUser(@RequestBody UserDTO user) {
		userService.addUser(user);
	}
	
	@RequestMapping(value = {"/user/{userId}"}, method = RequestMethod.GET) //1 api thu 2 tra ve chi tiet user
	public UserDTO viewUser( 
			@PathVariable(name="userId") int userId) {
		return  userService.getUserById(userId) ;
	}
}
