package com.trungtamjava.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.trungtamjava.model.UserDTO;
import com.trungtamjava.validator.UserValidator;

@Controller
public class HelloController {
	
	@Autowired
	private UserValidator userValidator;
	@RequestMapping("/say-hello")
	public ModelAndView sayHello1(HttpServletRequest request, HttpServletResponse response,HttpSession session,
				@RequestParam(name="user", required = true) String username,
				@RequestHeader(name="Accept", required = false) String contentType) {
		request.setAttribute("msg", contentType);
		return new ModelAndView("hello");
	}
	@RequestMapping("/hello/{name}/{id}")
	public String hello(HttpServletRequest request,
			@PathVariable(name="name") String name,
			@PathVariable(name="id") int userId) {
		
		request.setAttribute("msg", userId);
		return "hello";
	}
	
	@RequestMapping(value="/them-user", method = RequestMethod.GET)
	public String addUser(HttpServletRequest request) {
		UserDTO user = new UserDTO();
		user.setName("Spring");
		request.setAttribute("user", user);
		
		java.util.List<String> favourites = new ArrayList<String>();
		favourites.add("Xem phim");
		favourites.add("Nghe nhac");
		favourites.add("Coding");
		
		request.setAttribute("list", favourites);
		
		return "addUser";
	}
	
	@RequestMapping(value="/them-user", method = RequestMethod.POST)
	public String addUser(HttpServletRequest request, @ModelAttribute("user") UserDTO user,
			BindingResult bindingResult) {
		userValidator.validate(user, bindingResult);
		if(bindingResult.hasErrors()) {
			java.util.List<String> favourites = new ArrayList<String>();
			favourites.add("Xem phim");
			favourites.add("Nghe nhac");
			favourites.add("Coding");
			
			request.setAttribute("list", favourites);
			
			return "addUser";
		}
		
		MultipartFile file = user.getAvatar();
		try {
			File newFile = new File("D:/html-css/f8-shop/assets/image/" + file.getOriginalFilename());
			FileOutputStream fileOutputStream;
			fileOutputStream = new FileOutputStream(newFile);
			fileOutputStream.write(file.getBytes());
			fileOutputStream.close();	
			}catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (IOException e)  {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.setAttribute("u", user);
		return "viewUser";
	}
	
	@RequestMapping(value="/upload-file", method = RequestMethod.GET)
	public String upload(HttpServletRequest request) {
		
		return "upload";
	}
	@RequestMapping(value="/upload", method = RequestMethod.POST)
	public String upload(HttpServletRequest request, @RequestParam(name="file") java.util.List<MultipartFile> files) {		
		//Luu file xuong o cung	
		for (MultipartFile file : files) {
		try {
			File newFile = new File("D:/html-css/f8-shop/assets/image/" + file.getOriginalFilename());
			FileOutputStream fileOutputStream;
			fileOutputStream = new FileOutputStream(newFile);
			fileOutputStream.write(file.getBytes());
			fileOutputStream.close();	
			}catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (IOException e)  {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		request.setAttribute("files", files);
		return "viewFile";
	}
	
	@RequestMapping(value = "/download-file", method = RequestMethod.GET)
	public void download(HttpServletRequest request, HttpServletResponse response) {
		String dataDirectory = "D:/html-css/f8-shop/assets/image/";
		Path file = Paths.get(dataDirectory, "1.jpg");
		if (Files.exists(file)) {
			response.setContentType("image/jpg");
			response.addHeader("Content-Disposition", "attachment; filename= anh.jpg");
			try {
				Files.copy(file, response.getOutputStream());
				response.getOutputStream().flush();//đẩy dữ liệu từ response xuống client xong rồi rồi xóa đi 
			} catch (IOException ex) {
				// TODO: handle exception
				ex.printStackTrace();
			}
		}
	}
}
