package com.trungtamjava.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the PRODUCT database table.
 * 
 */
@Entity(name = "PRODUCT")
public class Product implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idPRODUCT")
	private int idPRODUCT;

	@Column(name = "name")
	private String name;

	@Column(name = "price")
	private int price;

	public Product() {
	}

	public int getIdPRODUCT() {
		return this.idPRODUCT;
	}

	public void setIdPRODUCT(int idPRODUCT) {
		this.idPRODUCT = idPRODUCT;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return this.price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

}