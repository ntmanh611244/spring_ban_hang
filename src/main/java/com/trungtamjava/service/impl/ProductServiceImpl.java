package com.trungtamjava.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.trungtamjava.dao.ProductDao;
import com.trungtamjava.dao.UserDao;
import com.trungtamjava.entity.Product;
import com.trungtamjava.model.ProductDTO;
import com.trungtamjava.service.ProductService;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {
	@Autowired
	private ProductDao productDao;
	
	@Override
	public void addProduct(ProductDTO pDto) {
		Product p = new Product();
		p.setName(pDto.getName());
		p.setPrice(pDto.getPrice());
		
		productDao.addProduct(p);
	}

	@Override
	public void updateProduct(ProductDTO pDto) {
		Product p = new Product();
		p.setName(pDto.getName());
		p.setPrice(pDto.getPrice());
		
		productDao.updateProduct(p);
	}

	@Override
	public void deleteProduct(int id) {
		productDao.deleteProduct(id);	
	}

	@Override
	public ProductDTO findById(int id) {
		Product p = productDao.getProductById(id);
		ProductDTO pDto = new ProductDTO();
		pDto.setId(id);
		pDto.setName(p.getName());
		pDto.setPrice(p.getPrice());
		
		return pDto;
	}

	@Override
	public List<ProductDTO> getAllProduct() {
		List<Product> listProducts = productDao.getAllProduct();
		List<ProductDTO> productDTOs = new ArrayList<ProductDTO>();
		for(Product p : listProducts) {
			ProductDTO pDto = new ProductDTO();
			pDto.setId(p.getIdPRODUCT());
			pDto.setName(p.getName());
			pDto.setPrice(p.getPrice());
			productDTOs.add(pDto);
		}
		return productDTOs;
	}

}
