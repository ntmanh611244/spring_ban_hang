package com.trungtamjava.service;

import java.util.List;

import com.trungtamjava.model.ProductDTO;

public interface ProductService {
	public void addProduct(ProductDTO pDto);
	public void updateProduct(ProductDTO pDto);
	public void deleteProduct(int id);
	public ProductDTO findById(int id);
	public List<ProductDTO> getAllProduct();
}
