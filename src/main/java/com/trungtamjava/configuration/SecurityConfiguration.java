package com.trungtamjava.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter{
	@Autowired
	javax.sql.DataSource dataSource;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().dataSource(dataSource)
		.usersByUsernameQuery("SELECT USERNAME, CONCAT('{noop}', PASSWORD), ENABLED FROM USER WHERE USERNAME = ?")
		.authoritiesByUsernameQuery("SELECT USERNAME, ROLE FROM USER WHERE USERNAME = ?");
	
		//auth.inMemoryAuthentication().passwordEncoder(NoOpPasswordEncoder.getInstance()).withUser("USER").password("PASSWORD");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable()/*Tinh nang chong tan cong o day khogn su dung nen disable di*/.authorizeRequests().antMatchers("/admin/**").hasRole("ADMIN")//co vai tro la admin thi se duoc truy cap vao duong dan antMatchers
		.and().authorizeRequests().antMatchers("/user/**").hasRole("USER")
		.anyRequest().permitAll().and().formLogin().loginPage("/dang-nhap").loginProcessingUrl("/login")// duong dan o jsp
		.usernameParameter("username").passwordParameter("password")// su dung 2 truong username va passsword trong jsp
		.defaultSuccessUrl("/danh-sach-san-pham").failureUrl("/dang-nhap?error=failed")
		.and().exceptionHandling().accessDeniedPage("/dang-nhap?error=deny");
	}

	 @Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/static/**");
	}
}
