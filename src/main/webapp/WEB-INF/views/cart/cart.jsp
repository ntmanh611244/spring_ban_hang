<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<h2>Danh sach gio hang</h2>
<table>
	<tr>
		<th>Ten:</th>
		<th>Gia</th>
		<th>So Luong</th>
		<th>Chon</th>
	</tr>
	<c:forEach items="${order.itemDTOs}" var="item">
		<tr>
			<td>${item.productDTO.name }</td>
			<td>${item.productDTO.price }</td>
			<td>${item.number }</td>
			<td><a href="<c:url value='/xoa-gio-hang/${item.productDTO.id }'/>"
				class="btn btn-primary">Xoa</a>
		</tr>
	</c:forEach>
</table>