<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>



<!-- The Modal -->
<div class="modal fade" id="myModal-add">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">Them khach hang</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<c:url value="/admin/them-khach-hang" var="url" />
				<form:form method="POST" action="${url}" modelAttribute="user">
					<p>Ten</p>
					<form:input path="name" />
					<p style="color: red;">
						<form:errors path="name"></form:errors>
					</p>
					<p>SDT</p>
					<form:input path="phone" />
					<br>
					<br>
					<button type="submit">Submit</button>
				</form:form>
			</div>
			<!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
