<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<h2>Danh sach nguoi dung</h2>
	<hr/>
	<table>
		<tr>
			<th>ID</th>
			<th>Ten</th>
			<th>So DT</th>
			<th>Chon</th>
		</tr>
		<c:forEach items="${users}" var="user">
		<tr>
			<td>${user.id }</td>
			<td>${user.name }</td>
			<td>${user.phone }</td>
			<td><a href="<c:url value='/admin/chi-tiet-khach-hang/${user.id }'/>">Chi tiet</a>
			<a href="<c:url value='/admin/xoa-khach-hang/${user.id }'/>">Xóa</a>
			<a href="<c:url value='/admin/sua-khach-hang/${user.id }'/>">Sửa</a>
			</td>
		</tr>
		</c:forEach>
	</table>
