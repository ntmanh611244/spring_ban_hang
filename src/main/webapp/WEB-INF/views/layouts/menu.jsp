<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>

<security:authorize access="isAuthenticated()">
<security:authentication property="principal" var="user"/>
	Welcome, ${user.username }
	<a href="<c:url  value="/dang-xuat"/>">Thoat</a>
</security:authorize>
<!--  	<a href="<c:url value='/them-khach-hang'/>">Th�m kh�ch h�ng</a>-->
	 <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal-add">
    Th�m kh�ch h�ng
  </button>
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal-edit">
    Sua nguoi dung
  </button>
	<a href="<c:url value='/admin/danh-sach-khach-hang'/>">Danh sach khach hang</a>
	<a href="<c:url value='/them-san-pham'/>">Them san pham</a>
	<a href="<c:url value='/danh-sach-san-pham'/>">Danh sach san pham</a>
	<a href="<c:url value='/xem-gio-hang'/>" class="btn btn-primary">Xem gio hang</a>