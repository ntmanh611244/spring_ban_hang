<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
 <h3>Welcome, Enter The Employee Details</h3>
 		<c:url value="/them-user" var="url" />
        <form:form method="POST" 
          action="${url}" modelAttribute="user" enctype="multipart/form-data">
         <p>Ten</p><form:input path="name"/>
         <p style="color: red;"><form:errors path="name"></form:errors></p>
         <p>Password</p><form:password path="password"/>
         <p style="color: red;"><form:errors path="password"></form:errors></p>
         <form:hidden path="id"/>
         <p>Sở thích</p><form:checkboxes path="favourites" items="${list }"/>
         <p>Giới tính</p>
         <form:select path="gender">
         	<form:option value="Nam">Nam</form:option>
         	<form:option value="Nữ">Nữ</form:option>
         </form:select>
         <p>Giới thiệu</p>
         <form:textarea path="about"/>
         <br/>
         <form:radiobutton path="acceptAgreement" value="true" label="Dong y dieu khoan su dung"/>
         <br/>
         <form:input path="avatar" type="file"/>
         <button type="submit">Submit</button>
        </form:form>
</body>
</html>