<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<p>Thông tin người dùng</p>
	<p>Tên: ${u.name}</p>
	<p>Password: ${u.password}</p>
	<p>Id: ${u.id}</p>
	<p>Giới tính: ${u.gender}</p>
	<p>Giới thiệu: ${u.about}</p>
	<p>Agreement: ${u.acceptAgreement}</p>
	<p>Sở thích:</p>
	<c:forEach items="${u.favourites }" var="item">
	  <p>${item }</p>
	</c:forEach>
	
	<p>Avatar: ${u.avatar.getOriginalFilename()}</p>
</body>
</html>