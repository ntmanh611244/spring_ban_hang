<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
${msg }
<table>
	<tr>
		<th>ID</th>
		<th>Ten</th>
		<th>Gia</th>
		<th>Chon</th>
	</tr>
	<c:forEach items="${products}" var="product">
		<tr>
			<td>${product.id }</td>
			<td>${product.name }</td>
			<td>${product.price }</td>
			<td><a href="<c:url value='/chi-tiet-san=pham/${product.id }'/>"
				class="btn btn-primary">Chi tiet</a> <a
				href="<c:url value='/xoa-san-pham/${product.id }'/>"
				class="btn btn-warning">Xóa</a> <a
				href="<c:url value='/sua-san-pham/${product.id }'/>"
				class="btn btn-danger">Sửa</a> <a
				href="<c:url value='/them-gio-hang/${product.id }'/>"
				class="btn btn-danger">Them vao gio hang</a></td>
		</tr>
	</c:forEach>
</table>