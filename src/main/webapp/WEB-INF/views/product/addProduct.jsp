<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<c:url value="/them-san-pham" var="url" />
	<f:form action="${url}" method="POST" modelAttribute="product">
		<p><spring:message code="product.name"/> </p>
		<f:input path="name"/>
		<p><spring:message code="product.price"/> </p>
		<f:input path="price"/>
		<br>
		<button type="submit" value="submit">Them </button>
	</f:form>
</body>
</html>